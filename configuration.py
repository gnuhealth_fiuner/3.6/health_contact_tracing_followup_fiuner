# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond import backend
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.model import MultiValueMixin, ValueMixin
from trytond.pool import Pool
from trytond.tools.multivalue import migrate_property
from trytond.pyson import Eval


__all__ = ['Configuration','Campaign',
           'ContactTracingENOMedicament',
           'ConfigurationContext',
           'Context']


class Configuration(ModelSingleton, ModelSQL, ModelView):
    'Configuration'
    __name__ = 'gnuhealth.contact_tracing.configuration'
    
    current_campaign = fields.Many2One(
        'gnuhealth.contact_tracing.campaign','Current Campaign',
        required = True)    
    contexts = fields.Many2Many(
        'gnuhealth.contact_tracing.configuration-contexts',
        'configuration','context','Contexts')


class Campaign(ModelSQL,ModelView):
    'Campaign'
    __name__ = 'gnuhealth.contact_tracing.campaign'    

    name = fields.Char('Name',required=True)
    condition = fields.Many2One('gnuhealth.pathology','Main condition to assess')
    suspect_condition = fields.Many2One('gnuhealth.pathology','Suspect Condition')
    eno_treatment_drugs = fields.Many2Many(
        'gnuhealth.contact_tracing-eno.medicament',
        'campaign','medicament','Treatment Drugs')
    

class ConfigurationContext(ModelSQL):
    'Configurtion - Context'
    __name__ = 'gnuhealth.contact_tracing.configuration-contexts'
    
    configuration = fields.Many2One(
        'gnuhealth.contact_tracing.configuration','Configuration',
        ondelete='CASCADE')
    context = fields.Many2One(
        'gnuhealth.contact_tracing.context','Context',
        ondelete='CASCADE')


class Context(ModelSQL,ModelView):
    'Context'
    __name__ = 'gnuhealth.contact_tracing.context'
    
    name = fields.Char('Context',required = True)
    active = fields.Boolean('Active')
    
    def get_rec_name(self, name):
        return self.name
    
    @classmethod
    def search_rec_name(cls, name, clause):
        return [('name',)+tuple(clause[1:])]


class ContactTracingENOMedicament(ModelSQL):
    'Treatment Drugs'
    __name__ = 'gnuhealth.contact_tracing-eno.medicament'
    
    medicament = fields.Many2One('gnuhealth.medicament','Medicament',
                               required=True,ondelete="CASCADE")
    
    campaign = fields.Many2One('gnuhealth.contact_tracing.campaign','Campaign', 
                               required=True,ondelete="CASCADE")
    
