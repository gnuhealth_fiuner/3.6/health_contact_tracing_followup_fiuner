-- RUN THIS SCRIPT BEFORE UPDATE THE MODULE JUST ONCE!

------ ### changing the field type and migration from boolean to string equivalence
-- ALTER TABLE gnuhealth_contact_tracing_call RENAME COLUMN anosmia TO anosmia_drop;
-- ALTER TABLE gnuhealth_contact_tracing_call RENAME COLUMN arthralgia TO arthralgia_drop;
-- ALTER TABLE gnuhealth_contact_tracing_call RENAME COLUMN diarrhea TO diarrhea_drop;
-- ALTER TABLE gnuhealth_contact_tracing_call RENAME COLUMN dysgeusia TO dysgeusia_drop;
-- ALTER TABLE gnuhealth_contact_tracing_call RENAME COLUMN dyspnoea TO dyspnoea_drop;
-- ALTER TABLE gnuhealth_contact_tracing_call RENAME COLUMN fever_high TO fever_high_drop;
-- ALTER TABLE gnuhealth_contact_tracing_call RENAME COLUMN myalgia TO myalgia_drop;
-- ALTER TABLE gnuhealth_contact_tracing_call RENAME COLUMN cough TO cough_drop;
-- ALTER TABLE gnuhealth_contact_tracing_call RENAME COLUMN headache TO headache_drop;

------####setting up conglomerate_institution from health institution to a party institution
-- ALTER TABLE gnuhealth_contact_tracing_eno RENAME COLUMN conglomerate_institution TO conglomerate_institution_drop;
-- SELECT gnuhealth_contact_tracing_eno.id, gnuhealth_institution.name as conglomerate_institution INTO new_values FROM gnuhealth_institution join gnuhealth_contact_tracing_eno ON gnuhealth_contact_tracing_eno.conglomerate_institution_drop = gnuhealth_institution.id;
 
-----#####change context and contact_type fields char into many2one selection widget
-- ALTER TABLE "gnuhealth_contact_tracing_eno-party" RENAME COLUMN contact_type TO contact_type_drop;


